import { Router } from "express";
import { readdirSync } from 'fs';

const PATH_ROUTER = `${__dirname}`;
const router = Router();
const files = readdirSync(PATH_ROUTER);
const cleanFileName = (fileName: string) => {
    return fileName.split('.').shift();
};

files.forEach(filename => {
    const cleanName = cleanFileName(filename);
    
    if (cleanName !== 'index') {
       import(`./${cleanName}`).then(moduleRouter => {
        router.use(`/${cleanName}`, moduleRouter.router);
       });
    }
});

export { router };

