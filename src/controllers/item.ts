import { Request, Response } from 'express';
import { deleteCar, getCar, getCars, insertCar, updateCar } from '../services/item.service';
import { handleHttp } from "../utils/error.handle";

const getItem = async (req: Request, res: Response) => {
    try {
        const {id} = req.params;
        const response = await getCar(id);
        const data = response ?? "NOT_FOUND";
        res.send(data)
    } catch (error) {
        handleHttp(res, 'ERROR_GET_ITEM');
    }
}
const getItems = async (req: Request, res: Response) => {
    try {
        const response = await getCars();
        res.send(response)
    } catch (error) {
        handleHttp(res, 'ERROR_GET_ITEMS');
    }
}
const updateItem = async (req: Request, res: Response) => {
    try {
        const {id} = req.params;
        const { body } = req;
        const response = await updateCar(id, body);
        res.send( response );
    } catch (error) {
        handleHttp(res, 'ERROR_UPDATE_ITEMS');
    }
}
const createItem = async (req: Request, res: Response) => {
    try {
        const { body } = req;        
        const responseItem = await insertCar(body);
        res.send( responseItem );
    } catch (error) {
        handleHttp(res, 'ERROR_CREATE_ITEM', error);
    }
}
const deleteItem = async (req: Request, res: Response) => {
    try {
        const {id} = req.params;
        const response = await deleteCar(id);
        res.send( response );
    } catch (error) {
        handleHttp(res, 'ERROR_DELETE_ITEM');
    }
}

export { createItem, deleteItem, getItem, getItems, updateItem };

