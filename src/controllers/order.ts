import { Response } from 'express';
import { RequestExt } from '../interfaces/requestExt.interface';
import { handleHttp } from "../utils/error.handle";

const getItems = async (req: RequestExt, res: Response) => {
    try {
        res.send({ 
            data: "ESTO SOLO LO VE LAS PERSONAS CON SESSION / JWT",
            user: req?.user
        })
    } catch (error) {
        handleHttp(res, 'ERROR_GET_ITEMS');
    }
}
export { getItems };

