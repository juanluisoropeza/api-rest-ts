import cors from "cors";
import "dotenv/config";
import express from "express";
import connectDatabase from './config/mongo';
import { router } from "./routes";

const PORT = process.env.PORT ?? 3001;

const app = express();
app.use(cors());
app.use(express.json());

//routes
app.use(router);

app.listen(PORT, () => {
    console.log('--------------- BACKEND NODEJS ----------------');
    console.log('------------ Listening port: '+PORT+' -------------');
});

const starDB = async () => {
    try {
      await connectDatabase();
    } catch (error) {
        console.error('Error connecting to the database:', error);
        process.exit(1);
      }
}
starDB()