import "dotenv/config";
import mongoose from "mongoose";

const envFile = process.env;
const MONGO_URI = `mongodb+srv://${envFile.MONGO_USER ?? ''}:${
  envFile.MONGO_PASS ?? ''
}@${envFile.MONGO_HOST ?? ''}/${
  envFile.MONGO_DB_NAME ?? ''
}?retryWrites=true&w=majority`;

const connectDatabase = async (): Promise<void> => {
    try {
      const db = await mongoose.connect(MONGO_URI);
      console.log(
        `----- Database: ${db.connection.db.databaseName} is conected! -----`
      );
    } catch (error) {
      console.error('Error connecting to the database:', error);
    }
  };
  
export default connectDatabase;