import { Schema, model } from "mongoose";
import { Car } from "../interfaces/car.interface";

const ItemSchema = new Schema<Car>(
    {
        name: {
            type: String,
            required: true
        },
        color: {
            type: String,
            required: true
        },
        gas: {
            type: String,
            required: true,
            enum: ["gasoline", "electric"]
        },
        year: {
            type: Number,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
    },
    {
        timestamps: true,
        versionKey: false
    }
);

const ItemModel = model<Car>("items", ItemSchema);

export default ItemModel;