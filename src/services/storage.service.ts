import { Storage } from "../interfaces/storage.interface";
import StorageModel from "../models/storage";


const registerUpload = async (dataToRegister: Storage) => {
  const responseItem = await StorageModel.create({ 
    fileName: dataToRegister.fileName, 
    idUser: dataToRegister.idUser, 
    path: dataToRegister.path 
  });
  return responseItem;
};

export { registerUpload };

