import { Auth } from "../interfaces/auth.interface";
import { User } from "../interfaces/user.interface";
import UserModel from "../models/user";
import { encrypt, verified } from "../utils/bcrypt.handle";
import { generateToken } from "../utils/jwt.handle";

const registerNewUser = async (newUser: User) => {
    const checkIs = await UserModel.findOne({email: newUser.email});
    if (checkIs) {
        return "ALREADY_USER";
    }
    const passHash = await encrypt(newUser.password);
    const user = {
        ...newUser,
        password: passHash
    };
    const registerUser = await UserModel.create(user);
    return registerUser;
};

const loginUser = async (userData: Auth) => {
    const checkIs = await UserModel.findOne({email: userData.email});
    if (!checkIs) {
        return "NOT_FOUND_USER";
    }
    const passwordHash = checkIs.password;
    const isCorrect = await verified(userData.password, passwordHash);
    if (!isCorrect) {
        return "PASSWORD_INCORRECT";
    }
    const token = await generateToken(checkIs.email);
    const data = {
        token: token,
        user: checkIs
    }
    return data;
};

export { loginUser, registerNewUser };

