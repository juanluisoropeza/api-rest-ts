import { NextFunction, Response } from "express";
import { RequestExt } from "../interfaces/requestExt.interface";
import { verifytoken } from "../utils/jwt.handle";

const checkJwt = async (req: RequestExt, res: Response, next: NextFunction): Promise<void> => {
    try {
        const jwtByUser = req.headers.authorization ?? "";
        const jwt = jwtByUser?.split(" ").pop();
        const vt = await verifytoken(`${jwt}`);
        if (!vt) {
            res.status(401).send("JWT_INVALID");
        } else {
            req.user = vt;
        }
        next();
    } catch (error) {
        res.status(400).send("SESSION_NOT_VALID")
    }
}

export { checkJwt };

